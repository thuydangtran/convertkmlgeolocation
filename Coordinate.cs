﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingKMLFile
{
    public class Coordinate
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
