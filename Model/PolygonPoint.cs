﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingKMLFile.Model
{
    public class PolygonPoint
    {
        public Guid Id { get; set; }
        public Guid ZoneId { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public int Seq { get; set; }
        public int ObjectState { get; set; }
    }
}
