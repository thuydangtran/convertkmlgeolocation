﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingKMLFile.Model
{
    public class Area
    {
        public List<PolygonPoint> PolygonPoints { get; set; }
    }
}
