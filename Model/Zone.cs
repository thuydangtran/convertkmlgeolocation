﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingKMLFile.Model
{
    public class Zone
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public string PostalCode { get; set; }
        public int ObjectState { get; set; }
        public List<Area> Areas { get; set; }
    }
}
