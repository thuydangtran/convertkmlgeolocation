﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingKMLFile.Model
{
    public class Region
    {
        public Guid Id { get; set; }
        public Guid ZoneId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int Seq { get; set; }
        public Zone Zone { get; set; }
        public int ObjectState { get; set; }
    }
}
