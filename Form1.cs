﻿using Newtonsoft.Json;
using ReadingKMLFile.OldModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace ReadingKMLFile
{
    public partial class Form1 : Form
    {
        public const string FILENAME = @"E:\Project\Getz\FileRegion\region.kml";
        public const string FILENAME_REGION = @"E:\Project\Getz\FileRegion\region_boundary_map.kml";
        public Form1()
        {
            InitializeComponent();
            //LoadXmlFile();

            //LoadMainRegion();
            //LoadSubzone("TS", "Tuas", ConstantRegion.subZoneTuas, "TUAS", "TS");
            //LoadSubzone("DT", "DownTownCore", ConstantRegion.subZoneDownTownCore, "DOWNTOWN CORE", "DT");
            //LoadSubzone("ME", "Maria", ConstantRegion.subZoneMariaEast, "MARINA EAST", "ME");
            //LoadSubzone("MS", "MariaSouth", ConstantRegion.subZoneMariaSouth, "MARINA SOUTH", "MS");
            //LoadSubzone("MU", "Mueseum", ConstantRegion.subZoneMuseum, "MUSEUM", "MU");
            //LoadSubzone("NT", "NewTon", ConstantRegion.subZoneNewTon, "NEWTON", "NT");
            //LoadSubzone("OR", "Orchard", ConstantRegion.subZoneOrchard, "ORCHARD", "OR");
            //LoadSubzone("OT", "Outram", ConstantRegion.subZoneOutram, "OUTRAM", "OT");
            //LoadSubzone("RV", "RiverValley", ConstantRegion.subZoneRiverValley, "RIVER VALLEY", "RV");
            //LoadSubzone("RC", "Rochor", ConstantRegion.subZoneRochor, "ROCHOR", "RC");
            //LoadSubzone("SR", "SingaporeRiver", ConstantRegion.subZoneSingaporeRiver, "SINGAPORE RIVER", "SR");
            //LoadSubzone("SV", "StraitsView", ConstantRegion.subZoneStraitsView, "STRAITS VIEW", "SV");
            //GenerateCBDZone();
            //ShowZone();
            LoadMainRegion();
        }

        public void LoadMainRegion()
        {
            var htmlDoc = new HtmlAgilityPack.HtmlDocument();
            XDocument doc = XDocument.Load(FILENAME_REGION);
            XElement root = doc.Root;
            XNamespace ns = root.GetDefaultNamespace();

            // get place mark
            var placemarkRegions = doc.Descendants(ns + "Placemark");

            // init list placeMark
            var listPlaceMark = new List<PlaceMark>();
            foreach(var region in ConstantRegion.regions)
            {
                var placeMark = new PlaceMark()
                {
                    RegionCode = region.Key,
                    RegionName = region.Value,
                    Polygons = new List<Polygon>()
                };

                listPlaceMark.Add(placeMark);
            }

            foreach(var placemark in placemarkRegions)
            {
                // get region code and region name
                var description = placemark.Descendants(ns + "description").FirstOrDefault().Value;
                htmlDoc.LoadHtml(description);
                var rows = htmlDoc.DocumentNode.SelectNodes("//tr")
                                               .Where(
                    child => child.ChildNodes.Where(x => x.Name.Equals("td")).FirstOrDefault().InnerText == "Region Code" 
                    || child.ChildNodes.Where(x => x.Name.Equals("td")).FirstOrDefault().InnerText == "Region Name"

                    )
                                               .Select(x => x);

                var tRowRegionName = htmlDoc.DocumentNode.SelectNodes("//td").Where(child => child.InnerText.Equals("Region Name")).FirstOrDefault().ParentNode;
                var tRowRegionCode = htmlDoc.DocumentNode.SelectNodes("//td").Where(child => child.InnerText.Equals("Region Code")).FirstOrDefault().ParentNode;

                var regionName = tRowRegionName.ChildNodes[3].InnerText;
                var regionCode = tRowRegionCode.ChildNodes[3].InnerText;

                var zone = listPlaceMark.FirstOrDefault(p => p.RegionCode.ToLower().Trim().Equals(regionCode.ToLower().Trim()));

                // get polygon
                var polygons = placemark.Descendants(ns + "Polygon");
                var listCoordinate = new List<Coordinate>();
                foreach(var item in polygons)
                {
                    var polygon = new Polygon()
                    {
                        Coordinates = new List<Coordinate>()
                    };

                    var boundary = item.Descendants(ns + "outerBoundaryIs").FirstOrDefault();

                    var geolocations = boundary.Descendants(ns + "coordinates").FirstOrDefault().Value.Trim();
                    var points = geolocations.Split(' ');
                    foreach (var point in points)
                    {
                        var coordinate = new Coordinate();
                        var coordinates = point.Split(',');
                        if (coordinates[1] != null)
                        {
                            coordinate.latitude = Convert.ToDouble(coordinates[1]);
                        }

                        if (coordinates[2] != null)
                        {
                            coordinate.longitude = Convert.ToDouble(coordinates[0]);
                        }

                        if (coordinates[1] != null && coordinates[0] != null)
                        {
                            listCoordinate.Add(coordinate);
                        }
                    }
                }


                this.ConvertToNewZoneModel(listCoordinate, $"{regionName}.json", regionName, regionCode);
            }

            //this.ConvertZoneFile(listPlaceMark);
        }

        public void ConvertZoneFile(List<PlaceMark> placeMarks)
        {
            var squenRegion = 0;
            var listRegion = new List<Model.Region>();
            foreach(var placeMark in placeMarks)
            {
                var zone = new Model.Zone
                {
                    Id = Guid.NewGuid(),
                    Name = placeMark.RegionName,
                    Code = placeMark.RegionCode,
                    PostalCode = string.Empty,
                    Areas = new List<Model.Area>()
                };

                foreach(var polygon in placeMark.Polygons)
                {
                    var area = new Model.Area
                    {
                        PolygonPoints = new List<Model.PolygonPoint>()
                    };

                    int squenPolygonPoint = 0;
                    foreach(var coordinate in polygon.Coordinates)
                    {
                        var polygonPoint = new Model.PolygonPoint
                        {
                            Id = Guid.NewGuid(),
                            Lat = Convert.ToDouble(coordinate.latitude),
                            Long = Convert.ToDouble(coordinate.longitude),
                            Seq = squenPolygonPoint++,
                            ZoneId = zone.Id,
                            ObjectState = 0
                        };

                        area.PolygonPoints.Add(polygonPoint);
                    }

                    zone.Areas.Add(area);
                }

                var region = new Model.Region
                {
                    Id = Guid.NewGuid(),
                    Latitude = string.Empty,
                    Longitude = string.Empty,
                    Seq = squenRegion++,
                    ZoneId = zone.Id,
                    Zone = zone
                };

                listRegion.Add(region);
            }

            if(File.Exists("Geo.js"))
            {
                File.Delete("Geo.js");
            }

            using (var tw = new StreamWriter("Geo.js", true))
            {
                tw.WriteLine(JsonConvert.SerializeObject(listRegion, Newtonsoft.Json.Formatting.Indented));
            }
        }

        private void GetAllSubZone(string planningAreaCode)
        {
            XDocument doc = XDocument.Load(FILENAME);
            XElement root = doc.Root;
            XNamespace ns = root.GetDefaultNamespace();

            // get list placemark
            var placemarks = doc.Descendants(ns + "Placemark");

            var plannings = from p in placemarks
                            where p.Descendants(ns + "ExtendedData")
                                   .Descendants(ns + "SchemaData")
                                   .Descendants(ns + "SimpleData")
                                   .Where(x => x.Attribute("name").Value == "PLN_AREA_C")
                                   .Select(q => q.Value).Distinct().FirstOrDefault().Trim().ToUpper() == planningAreaCode.Trim().ToUpper()
                            select p;

            // show name region
            System.Diagnostics.Debug.WriteLine("List Sub Zone: ");
            foreach (var planning in plannings)
            {
                var simpleData = planning.Descendants(ns + "ExtendedData")
                                      .Descendants(ns + "SchemaData")
                                      .Descendants(ns + "SimpleData");

                var subZoneName = simpleData.Where(x => x.Attribute("name").Value == "SUBZONE_N")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                System.Diagnostics.Debug.WriteLine(subZoneName);
            }

            var listZone = "List Sub Zone : \n";
            var dictionListZone = new SortedDictionary<string, string>();
            foreach (var item in plannings)
            {
                var simpleData = item.Descendants(ns + "ExtendedData")
                                      .Descendants(ns + "SchemaData")
                                      .Descendants(ns + "SimpleData");


                var subZoneCode = simpleData.Where(x => x.Attribute("name").Value == "SUBZONE_C")
                                      .Select(q => q.Value).Distinct().FirstOrDefault();
                var subZoneName = simpleData.Where(x => x.Attribute("name").Value == "SUBZONE_N")
                      .Select(q => q.Value).Distinct().FirstOrDefault();
                dictionListZone.Add(subZoneCode, subZoneName);

            }

            var showList = string.Empty;
            foreach(var item in dictionListZone)
            {
                showList += $"{item.Key} - {item.Value}\n";
            }

            showList = string.IsNullOrEmpty(showList) ? "Not Found" : showList;
            listZone += showList;
            txtResultSubZone.Text = listZone;
            System.Diagnostics.Debug.WriteLine(listZone);
        }

        public void LoadSubzone(string planningAreaCode, string fileName, Dictionary<string, string> subZone, string regionZoneName, string regionZoneCode)
        {
            XDocument doc = XDocument.Load(FILENAME);
            XElement root = doc.Root;
            XNamespace ns = root.GetDefaultNamespace();

            // get list placemark
            var placemarks = doc.Descendants(ns + "Placemark");

            var plannings = from p in placemarks
                               where p.Descendants(ns + "ExtendedData")
                                      .Descendants(ns + "SchemaData")
                                      .Descendants(ns + "SimpleData")
                                      .Where(x => x.Attribute("name").Value == "PLN_AREA_C")
                                      .Select(q => q.Value).Distinct().FirstOrDefault().Trim().ToUpper() == planningAreaCode.Trim().ToUpper()
                               select p;

            // show name region
            System.Diagnostics.Debug.WriteLine("List Sub Zone: ");
            foreach(var planning in plannings)
            {
                var simpleData = planning.Descendants(ns + "ExtendedData")
                                      .Descendants(ns + "SchemaData")
                                      .Descendants(ns + "SimpleData");

                var subZoneName = simpleData.Where(x => x.Attribute("name").Value == "SUBZONE_N")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                System.Diagnostics.Debug.WriteLine(subZoneName);
            }



            // init placeMark
            var listTotalCoordinate = new List<Coordinate>();
            var listPlaceMark = new List<PlaceMark>();
            foreach (var planning in subZone)
            {
                var placemark = new PlaceMark()
                {
                    SubZoneCode = planning.Key,
                    SubZoneName = planning.Value,
                    PlanningAreaCode = string.Empty,
                    PlanningAreaName = string.Empty,
                    RegionCode = string.Empty,
                    RegionName = string.Empty,
                    Polygons = new List<Polygon>()
                };

                listPlaceMark.Add(placemark);
            }

            // set data for item in list place mark
            foreach(var item in plannings)
            {
                var simpleData = item.Descendants(ns + "ExtendedData")
                                      .Descendants(ns + "SchemaData")
                                      .Descendants(ns + "SimpleData");


                var subZoneCode = simpleData.Where(x => x.Attribute("name").Value == "SUBZONE_C")
                                      .Select(q => q.Value).Distinct().FirstOrDefault();
                var subZoneName = simpleData.Where(x => x.Attribute("name").Value == "SUBZONE_N")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                var planingAreaCode = simpleData.Where(x => x.Attribute("name").Value == "PLN_AREA_C")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                var planingAreaName = simpleData.Where(x => x.Attribute("name").Value == "PLN_AREA_N")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                var regionCode = simpleData.Where(x => x.Attribute("name").Value == "REGION_C")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                var regionName = simpleData.Where(x => x.Attribute("name").Value == "REGION_N")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                // get place mark from the init list
                var placeMark = listPlaceMark.FirstOrDefault(p => p.SubZoneCode == subZoneCode);

                if(placeMark != null)
                {
                    placeMark.PlanningAreaName = planingAreaName;
                    placeMark.PlanningAreaCode = planingAreaCode;
                    placeMark.RegionCode = regionCode;
                    placeMark.RegionName = regionName;


                    var polygons = item.Descendants(ns + "Polygon");
                    var polygon = polygons.FirstOrDefault();
                    System.Diagnostics.Debug.WriteLine(polygons.Count());
                    var polygonItem = new Polygon
                    {
                        Coordinates = new List<Coordinate>()
                    };

                    var coordinateElement = polygon.Descendants(ns + "coordinates").FirstOrDefault();
                    var polygonPoint = coordinateElement.Value.Split(' ').ToList();
                    var listPointPoly = new List<Coordinate>();
                    foreach (var point in polygonPoint)
                    {
                        var arrPoint = point.Split(',');
                        var coordinates = new Coordinate();
                        if (arrPoint[1] != null)
                        {
                            coordinates.latitude = Convert.ToDouble(arrPoint[1].Trim().ToString());
                        }

                        if (arrPoint[0] != null)
                        {
                            coordinates.longitude = Convert.ToDouble(arrPoint[0].Trim().ToString());
                        }

                        if (arrPoint[1] != null && arrPoint[0] != null)
                        {

                            polygonItem.Coordinates.Add(coordinates);
                            listPointPoly.Add(coordinates);
                        }
                    }
                    listTotalCoordinate.AddRange(listPointPoly);
                    placeMark.Polygons.Add(polygonItem);
                }
                
            }

            var listSumCoordinate = new List<List<Coordinate>>();
            foreach (var item in listPlaceMark)
            {
                var polygon = item.Polygons.FirstOrDefault();
                listSumCoordinate.Add(polygon.Coordinates);
            }

            var totalDup = 0;
            var listPoint = new List<Coordinate>();
            foreach (var listSubZoneCoordinate in listSumCoordinate)
            {
                foreach (var point in listSubZoneCoordinate)
                {
                    if (listPoint.Any(p => p.latitude.Equals(point.latitude) && p.longitude.Equals(point.longitude)))
                    {
                        var pointRemove = listPoint.FirstOrDefault(p => p.latitude.Equals(point.latitude) && p.longitude.Equals(point.longitude));
                        listPoint.Remove(pointRemove);
                        totalDup++;
                    }
                    else
                    {
                        listPoint.Add(point);
                    }
                }
            }
            System.Diagnostics.Debug.WriteLine($"Dup Point For Zone {regionZoneName}: " + totalDup.ToString());
            System.Diagnostics.Debug.WriteLine("Total Point: " + listPoint.Count.ToString());
            this.ConvertSubZoneToOneRegion(listPoint, $"{fileName}.js", regionZoneName, regionZoneCode);
        }

        public void ConvertSubZoneToOneRegion(List<Coordinate> coordinates, string fileName, string regionName, string regionCode)
        {
            var zone = new ZoneOld
            {
                Id = Guid.NewGuid(),
                Name = regionName,
                Code = regionCode,
                PolygonPoints = new List<Model.PolygonPoint>()
            };

            var countSeqPolygon = 0;
            foreach(var item in coordinates)
            {
                var polygonPoint = new Model.PolygonPoint()
                {
                    Id = Guid.NewGuid(),
                    Lat = Convert.ToDouble(item.latitude),
                    Long = Convert.ToDouble(item.longitude),
                    Seq = countSeqPolygon++,
                    ZoneId = zone.Id
                };
                zone.PolygonPoints.Add(polygonPoint);
            }

            var region = new OldModel.RegionOld
            {
                Id = Guid.NewGuid(),
                Zone = zone,
                ZoneId = zone.Id
            };

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            using (var tw = new StreamWriter(fileName, true))
            {
                tw.WriteLine(JsonConvert.SerializeObject(region, Newtonsoft.Json.Formatting.Indented));
            }
        }

        public void ConvertToNewZoneModel(List<Coordinate> coordinates, string fileName, string regionName, string regionCode)
        {
            var zone = new Zones
            {
                zoneId = Guid.NewGuid(),
                points = coordinates
            };

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            using (var tw = new StreamWriter(fileName, true))
            {
                tw.WriteLine(JsonConvert.SerializeObject(zone, Newtonsoft.Json.Formatting.Indented));
            }
        }
        private void btnPlanningCode_Click(object sender, EventArgs e)
        {
            // get all SubZone
            GetAllSubZone(txtPlanningCode.Text.Trim());
        }

        private List<Coordinate> GetListCoordinateSubZOne(string planningAreaCode, Dictionary<string, string> subZone)
        {
            XDocument doc = XDocument.Load(FILENAME);
            XElement root = doc.Root;
            XNamespace ns = root.GetDefaultNamespace();

            // get list placemark
            var placemarks = doc.Descendants(ns + "Placemark");

            var plannings = from p in placemarks
                            where p.Descendants(ns + "ExtendedData")
                                   .Descendants(ns + "SchemaData")
                                   .Descendants(ns + "SimpleData")
                                   .Where(x => x.Attribute("name").Value == "PLN_AREA_C")
                                   .Select(q => q.Value).Distinct().FirstOrDefault().Trim().ToUpper() == planningAreaCode.Trim().ToUpper()
                            select p;

            // show name region
            System.Diagnostics.Debug.WriteLine("List Sub Zone: ");
            foreach (var planning in plannings)
            {
                var simpleData = planning.Descendants(ns + "ExtendedData")
                                      .Descendants(ns + "SchemaData")
                                      .Descendants(ns + "SimpleData");

                var subZoneName = simpleData.Where(x => x.Attribute("name").Value == "SUBZONE_N")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                System.Diagnostics.Debug.WriteLine(subZoneName);
            }



            // init placeMark
            var listTotalCoordinate = new List<Coordinate>();
            var listPlaceMark = new List<PlaceMark>();
            foreach (var planning in subZone)
            {
                var placemark = new PlaceMark()
                {
                    SubZoneCode = planning.Key,
                    SubZoneName = planning.Value,
                    PlanningAreaCode = string.Empty,
                    PlanningAreaName = string.Empty,
                    RegionCode = string.Empty,
                    RegionName = string.Empty,
                    Polygons = new List<Polygon>()
                };

                listPlaceMark.Add(placemark);
            }

            // set data for item in list place mark
            foreach (var item in plannings)
            {
                var simpleData = item.Descendants(ns + "ExtendedData")
                                      .Descendants(ns + "SchemaData")
                                      .Descendants(ns + "SimpleData");


                var subZoneCode = simpleData.Where(x => x.Attribute("name").Value == "SUBZONE_C")
                                      .Select(q => q.Value).Distinct().FirstOrDefault();
                var subZoneName = simpleData.Where(x => x.Attribute("name").Value == "SUBZONE_N")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                var planingAreaCode = simpleData.Where(x => x.Attribute("name").Value == "PLN_AREA_C")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                var planingAreaName = simpleData.Where(x => x.Attribute("name").Value == "PLN_AREA_N")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                var regionCode = simpleData.Where(x => x.Attribute("name").Value == "REGION_C")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                var regionName = simpleData.Where(x => x.Attribute("name").Value == "REGION_N")
                      .Select(q => q.Value).Distinct().FirstOrDefault();

                // get place mark from the init list
                var placeMark = listPlaceMark.FirstOrDefault(p => p.SubZoneCode == subZoneCode);

                if (placeMark != null)
                {
                    placeMark.PlanningAreaName = planingAreaName;
                    placeMark.PlanningAreaCode = planingAreaCode;
                    placeMark.RegionCode = regionCode;
                    placeMark.RegionName = regionName;


                    var polygons = item.Descendants(ns + "Polygon");
                    var polygon = polygons.FirstOrDefault();
                    System.Diagnostics.Debug.WriteLine(polygons.Count());
                    var polygonItem = new Polygon
                    {
                        Coordinates = new List<Coordinate>()
                    };

                    var coordinateElement = polygon.Descendants(ns + "coordinates").FirstOrDefault();
                    var polygonPoint = coordinateElement.Value.Split(' ').ToList();
                    var listPointPoly = new List<Coordinate>();
                    foreach (var point in polygonPoint)
                    {
                        var arrPoint = point.Split(',');
                        var coordinates = new Coordinate();
                        if (arrPoint[1] != null)
                        {
                            coordinates.latitude = Convert.ToDouble(arrPoint[1].Trim().ToString());
                        }

                        if (arrPoint[0] != null)
                        {
                            coordinates.longitude = Convert.ToDouble(arrPoint[0].Trim().ToString());
                        }

                        if (arrPoint[1] != null && arrPoint[0] != null)
                        {

                            polygonItem.Coordinates.Add(coordinates);
                            listPointPoly.Add(coordinates);
                        }
                    }
                    listTotalCoordinate.AddRange(listPointPoly);
                    placeMark.Polygons.Add(polygonItem);
                }

            }

            var listSumCoordinate = new List<List<Coordinate>>();
            foreach (var item in listPlaceMark)
            {
                var polygon = item.Polygons.FirstOrDefault();
                listSumCoordinate.Add(polygon.Coordinates);
            }

            var totalDup = 0;
            var listPoint = new List<Coordinate>();
            foreach (var listSubZoneCoordinate in listSumCoordinate)
            {
                foreach (var point in listSubZoneCoordinate)
                {
                    if (listPoint.Any(p => p.latitude.Equals(point.latitude) && p.longitude.Equals(point.longitude)))
                    {
                        var pointRemove = listPoint.FirstOrDefault(p => p.latitude.Equals(point.latitude) && p.longitude.Equals(point.longitude));
                        listPoint.Remove(pointRemove);
                        totalDup++;
                    }
                    else
                    {
                        listPoint.Add(point);
                    }
                }
            }
            System.Diagnostics.Debug.WriteLine("Total Dup Point : " + totalDup.ToString());
            System.Diagnostics.Debug.WriteLine($"{planningAreaCode} Point : " + listPoint.Count.ToString());

            return listPoint;
        }

        /// <summary>
        /// Get CBD Zone (Downtown Core, maria, Newton,....)
        /// </summary>
        /// <returns></returns>
        private List<Coordinate> GetCBDZone()
        {
            var listSubZone = new List<List<Coordinate>>();
            foreach(var item in ConstantRegion.cbdSubZone)
            {
                var subZone = this.GetListCoordinateSubZOne(item.Key, item.Value);
                listSubZone.Add(subZone);
            }

            var totalDup = 0;
            var listPoint = new List<Coordinate>();
            foreach(var item in listSubZone)
            {
                foreach(var point in item)
                {
                    if (listPoint.Any(p => p.latitude.Equals(point.latitude) && p.longitude.Equals(point.longitude)))
                    {
                        var pointRemove = listPoint.FirstOrDefault(p => p.latitude.Equals(point.latitude) && p.longitude.Equals(point.longitude));
                        listPoint.Remove(pointRemove);
                        totalDup++;
                    }
                    else
                    {
                        listPoint.Add(point);
                    }
                }
            }

            System.Diagnostics.Debug.WriteLine("Total Dup Point : " + totalDup.ToString());
            System.Diagnostics.Debug.WriteLine("CBD Point : " + listPoint.Count.ToString());
            this.ConvertToNewZoneModel(listPoint, "CBD.js", "Central Business District", "CBD");
            return listPoint;
        }

        private List<Coordinate> GetListCoordinateSingleSubZone(string subZoneCode)
        {
            XDocument doc = XDocument.Load(FILENAME);
            XElement root = doc.Root;
            XNamespace ns = root.GetDefaultNamespace();

            // get list placemark
            var placemarks = doc.Descendants(ns + "Placemark");

            var subZones = from p in placemarks
                            where p.Descendants(ns + "ExtendedData")
                                   .Descendants(ns + "SchemaData")
                                   .Descendants(ns + "SimpleData")
                                   .Where(x => x.Attribute("name").Value == "SUBZONE_C")
                                   .Select(q => q.Value).Distinct().FirstOrDefault().Trim().ToUpper() == subZoneCode.Trim().ToUpper()
                            select p;

            var zone = subZones.FirstOrDefault();

            var infoZonedatas = zone.Descendants(ns + "ExtendedData")
                                    .Descendants(ns + "SchemaData")
                                    .Descendants(ns + "SimpleData");

            var subZoneName = infoZonedatas.Where(x => x.Attribute("name").Value == "SUBZONE_N")
                  .Select(q => q.Value).Distinct().FirstOrDefault();

            var planingAreaCode = infoZonedatas.Where(x => x.Attribute("name").Value == "PLN_AREA_C")
                  .Select(q => q.Value).Distinct().FirstOrDefault();

            var planingAreaName = infoZonedatas.Where(x => x.Attribute("name").Value == "PLN_AREA_N")
                  .Select(q => q.Value).Distinct().FirstOrDefault();

            var regionCode = infoZonedatas.Where(x => x.Attribute("name").Value == "REGION_C")
                  .Select(q => q.Value).Distinct().FirstOrDefault();

            var regionName = infoZonedatas.Where(x => x.Attribute("name").Value == "REGION_N")
                  .Select(q => q.Value).Distinct().FirstOrDefault();


            // get polygon
            var polygon = zone.Descendants(ns + "Polygon").FirstOrDefault();

            var coordinateElement = polygon.Descendants(ns + "coordinates").FirstOrDefault();
            var polygonPoint = coordinateElement.Value.Split(' ').ToList();
            var listPointPoly = new List<Coordinate>();
            foreach (var point in polygonPoint)
            {
                var arrPoint = point.Split(',');
                var coordinates = new Coordinate();
                if (arrPoint[1] != null)
                {
                    coordinates.latitude = Convert.ToDouble(arrPoint[1].Trim().ToString());
                }

                if (arrPoint[0] != null)
                {
                    coordinates.longitude = Convert.ToDouble(arrPoint[0].Trim().ToString());
                }

                if (arrPoint[0] != null && arrPoint[1] != null)
                {
                    listPointPoly.Add(coordinates);
                }
            }

            System.Diagnostics.Debug.WriteLine($"Total {subZoneName} points: {listPointPoly.Count}");
            return listPointPoly;
        }



        private OldModel.RegionOld GetRegion(List<Coordinate> coordinates, string regionName, string regionCode, Coordinate regionCoordinate)
        {
            var zone = new ZoneOld
            {
                Id = Guid.NewGuid(),
                Name = regionName,
                Code = regionCode,
                PolygonPoints = new List<Model.PolygonPoint>()
            };

            var countSeqPolygon = 0;
            foreach (var item in coordinates)
            {
                var polygonPoint = new Model.PolygonPoint()
                {
                    Id = Guid.NewGuid(),
                    Lat = Convert.ToDouble(item.latitude),
                    Long = Convert.ToDouble(item.longitude),
                    Seq = countSeqPolygon++,
                    ZoneId = zone.Id
                };
                zone.PolygonPoints.Add(polygonPoint);
            }

            var region = new OldModel.RegionOld
            {
                Id = Guid.NewGuid(),
                Zone = zone,
                Lat = Convert.ToDouble(regionCoordinate.latitude), 
                Long = Convert.ToDouble(regionCoordinate.longitude),
                ZoneId = zone.Id
            };

            return region;
        }

        private Zones GetZones(List<Coordinate> coordinates, string regionName, string regionCode, Coordinate regionCoordinate)
        {
            var zone = new Zones
            {
                zoneId = Guid.NewGuid(),
                zoneName = regionName,
                zoneCode = regionCode,
                points = coordinates
            };

            return zone;
        }

        private void WriteRegionToFile(List<Zones> listRegion, bool isMiniFile = false, string fileName = "newZone.js")
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            using (var tw = new StreamWriter(fileName, true))
            {
                if(!isMiniFile)
                {
                    tw.WriteLine(JsonConvert.SerializeObject(listRegion, Newtonsoft.Json.Formatting.Indented));
                }
                else
                {
                    tw.WriteLine(JsonConvert.SerializeObject(listRegion));
                }

            }
        }

        private Coordinate GetRegionCoordinate(string regionCode)
        {
            var item = ConstantRegion.regionCoordinate.FirstOrDefault(r => r.Key.Trim().Equals(regionCode.Trim())).Value;
            return item;
        }

        private void ShowZone()
        {
            // 4 zone single
            var listRegions = new List<Zones>();
            foreach(var item in ConstantRegion.subZoneSingle)
            {
                var subZonePoints = this.GetListCoordinateSingleSubZone(item.Key);
                var zoneCoordinate = this.GetRegionCoordinate(item.Key);
                var subZoneRegion = this.GetZones(subZonePoints, item.Value, item.Key, zoneCoordinate);
                listRegions.Add(subZoneRegion);

                // write file
                var fileName = item.Value.Replace(' ', '_');
                this.ConvertToNewZoneModel(subZonePoints, $"{fileName}.js", item.Value, item.Key);

            }

            // Tuas Zone
            var tuas = ConstantRegion.tuasZone.FirstOrDefault();
            var listTuasPoints = this.GetListCoordinateSubZOne(tuas.Key, tuas.Value);
            var tuasCoordinate = this.GetRegionCoordinate(tuas.Key);
            var tuasRegion = this.GetZones(listTuasPoints, "TUAS", tuas.Key, tuasCoordinate);
            listRegions.Add(tuasRegion);
            this.ConvertToNewZoneModel(listTuasPoints, $"TUAS.js", "TUAS", tuas.Key);

            // CBD Zone
            var listcbdPoints = this.GetCBDZone();
            var cbdCoordinate = this.GetRegionCoordinate("CBD");
            var cbdRegions = this.GetZones(listcbdPoints, "Central Business District".ToUpper(), "CBD", cbdCoordinate);
            listRegions.Add(cbdRegions);

            this.WriteRegionToFile(listRegions);
        }
    }
}
