﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingKMLFile
{
    public class PlaceMark
    {
        public string SubZoneName { get; set; }
        public string SubZoneCode { get; set; }
        public string PlanningAreaName { get; set; }
        public string PlanningAreaCode { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public List<Polygon> Polygons { get; set; }
    }
}
