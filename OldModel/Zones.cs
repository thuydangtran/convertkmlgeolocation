﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingKMLFile.OldModel
{
    public class Zones
    {
        public Guid zoneId { get; set; }
        public string zoneName { get; set; }
        public string zoneCode { get; set; }
        public List<Coordinate> points { get; set; }
    }
}
