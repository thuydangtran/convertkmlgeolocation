﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingKMLFile.OldModel
{
    public class RegionOld
    {
        public Guid Id { get; set; }
        public Guid ZoneId { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public int Seq { get; set; }
        public ZoneOld Zone { get; set; }
        public int ObjectState { get; set; }
    }
}
