﻿namespace ReadingKMLFile
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPlanningCode = new System.Windows.Forms.TextBox();
            this.txtResultSubZone = new System.Windows.Forms.RichTextBox();
            this.btnPlanningCode = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtPlanningCode
            // 
            this.txtPlanningCode.Location = new System.Drawing.Point(150, 51);
            this.txtPlanningCode.Name = "txtPlanningCode";
            this.txtPlanningCode.Size = new System.Drawing.Size(290, 20);
            this.txtPlanningCode.TabIndex = 0;
            // 
            // txtResultSubZone
            // 
            this.txtResultSubZone.Location = new System.Drawing.Point(29, 133);
            this.txtResultSubZone.Name = "txtResultSubZone";
            this.txtResultSubZone.Size = new System.Drawing.Size(502, 185);
            this.txtResultSubZone.TabIndex = 1;
            this.txtResultSubZone.Text = "";
            // 
            // btnPlanningCode
            // 
            this.btnPlanningCode.Location = new System.Drawing.Point(456, 51);
            this.btnPlanningCode.Name = "btnPlanningCode";
            this.btnPlanningCode.Size = new System.Drawing.Size(75, 23);
            this.btnPlanningCode.TabIndex = 2;
            this.btnPlanningCode.Text = "Find";
            this.btnPlanningCode.UseVisualStyleBackColor = true;
            this.btnPlanningCode.Click += new System.EventHandler(this.btnPlanningCode_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Planning Zone Code: ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 369);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnPlanningCode);
            this.Controls.Add(this.txtResultSubZone);
            this.Controls.Add(this.txtPlanningCode);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPlanningCode;
        private System.Windows.Forms.RichTextBox txtResultSubZone;
        private System.Windows.Forms.Button btnPlanningCode;
        private System.Windows.Forms.Label label1;
    }
}

