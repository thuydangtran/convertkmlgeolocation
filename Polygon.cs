﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingKMLFile
{
    public class Polygon
    {
        public List<Coordinate> Coordinates { get; set; }
    }
}
