﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingKMLFile
{
    public static class ConstantRegion
    {
        public static Dictionary<string, string> regions = new Dictionary<string, string>()
        {
            {"CR", "CENTRAL REGION" },
            {"WR", "WEST REGION"},
            {"NR", "NORTH REGION" },
            {"ER", "EAST REGION" },
            {"NER", "NORTH-EAST REGION" },
        };

        public static Dictionary<string, string> subZoneTuas = new Dictionary<string, string>()
        {
            {"TSSZ01",  "TENGEH"},
            {"TSSZ02",  "TUAS PROMENADE"},
            {"TSSZ03",  "TUAS NORTH"},
            {"TSSZ04",  "TUAS BAY"},
            {"TSSZ05",  "TUAS VIEW"},
            {"TSSZ06",  "TUAS VIEW EXTENSION"}
        };


        public static Dictionary<string, string> subZoneDownTownCore = new Dictionary<string, string>()
        {
            {"DTSZ01", "BUGIS"},
            {"DTSZ02", "CITY HALL"},
            {"DTSZ03", "MARINA CENTRE"},
            {"DTSZ04", "PHILLIP"},
            {"DTSZ05", "RAFFLES PLACE"},
            {"DTSZ06", "CLIFFORD PIER"},
            {"DTSZ07", "MAXWELL"},
            {"DTSZ08", "CECIL"},
            {"DTSZ09", "TANJONG PAGAR"},
            {"DTSZ10", "ANSON"},
            {"DTSZ11", "CENTRAL SUBZONE"},
            {"DTSZ12", "BAYFRONT SUBZONE"},
            {"DTSZ13", "NICOLL"}
        };

        public static Dictionary<string, string> subZoneMariaEast = new Dictionary<string, string>()
        {
            {"MESZ01",  "MARINA EAST"}
        };

        public static Dictionary<string, string> subZoneMariaSouth = new Dictionary<string, string>()
        {
            {"MSSZ01",  "MARINA SOUTH"}
        };

        public static Dictionary<string, string> subZoneMuseum = new Dictionary<string, string>()
        {
            {"MUSZ01", "DHOBY GHAUT" },
            {"MUSZ02" , "FORT CANNING"},
            {"MUSZ03",  "BRAS BASAH"}
        };

        public static Dictionary<string, string> subZoneNewTon = new Dictionary<string, string>()
        {
            {"NTSZ01", "ORANGE GROVE" },
            {"NTSZ02" , "GOODWOOD PARK"},
            {"NTSZ03",  "NEWTON CIRCUS"},
            {"NTSZ04",  "CAIRNHILL"},
            {"NTSZ05",  "MONK'S HILL"},
            {"NTSZ06",  "ISTANA NEGARA"},
        };

        public static Dictionary<string, string> subZoneOrchard = new Dictionary<string, string>()
        {
            {"ORSZ01", "TANGLIN" },
            {"ORSZ02" , "BOULEVARD"},
            {"ORSZ03",  "SOMERSET"},
        };

        public static Dictionary<string, string> subZoneOutram = new Dictionary<string, string>()
        {
            {"OTSZ01", "PEARL'S HILL" },
            {"OTSZ02" , "PEOPLE'S PARK"},
            {"OTSZ03",  "CHINATOWN"},
            {"OTSZ04",  "CHINA SQUARE"},
        };


        public static Dictionary<string, string> subZoneRiverValley = new Dictionary<string, string>()
        {
            {"RVSZ01", "ONE TREE HILL" },
            {"RVSZ02" , "PATERSON"},
            {"RVSZ03",  "LEONIE HILL"},
            {"RVSZ04",  "OXLEY"},
            {"RVSZ05",  "INSTITUTION HILL"},
        };


        public static Dictionary<string, string> subZoneRochor = new Dictionary<string, string>()
        {
            {"RCSZ01", "FARRER PARK" },
            {"RCSZ02" , "LITTLE INDIA"},
            {"RCSZ03",  "SUNGEI ROAD"},
            {"RCSZ04",  "ROCHOR CANAL"},
            {"RCSZ05",  "KAMPONG GLAM"},
            {"RCSZ06",  "MACKENZIE"},
            {"RCSZ07",  "MOUNT EMILY"},
            {"RCSZ08",  "SELEGIE"},
            {"RCSZ09",  "BENCOOLEN"},
            {"RCSZ10",  "VICTORIA"},
        };


        public static Dictionary<string, string> subZoneSingaporeRiver = new Dictionary<string, string>()
        {
            {"SRSZ01", "ROBERTSON QUAY" },
            {"SRSZ02" , "CLARKE QUAY"},
            {"SRSZ03",  "BOAT QUAY"},
        };

        public static Dictionary<string, string> subZoneStraitsView = new Dictionary<string, string>()
        {
            {"SVSZ01", "STRAITS VIEW" }
        };


        public static Dictionary<string, Dictionary<string, string>> cbdSubZone = new Dictionary<string, Dictionary<string, string>>
        {
            {"DT", ConstantRegion.subZoneDownTownCore },
            {"ME", ConstantRegion.subZoneMariaEast },
            {"MS", ConstantRegion.subZoneMariaSouth },
            {"SV", ConstantRegion.subZoneStraitsView },
            {"OT", ConstantRegion.subZoneOutram },
            {"SR", ConstantRegion.subZoneSingaporeRiver },
            {"RV", ConstantRegion.subZoneRiverValley },
            {"OR", ConstantRegion.subZoneOrchard },
            {"NT", ConstantRegion.subZoneNewTon },
            {"RC", ConstantRegion.subZoneRochor },
            {"MU", ConstantRegion.subZoneMuseum }
        };

        public static Dictionary<string, Dictionary<string, string>> tuasZone = new Dictionary<string, Dictionary<string, string>>
        {
            {"TS", ConstantRegion.subZoneTuas }
        };

        public static Dictionary<string, string> subZoneLimChuKang = new Dictionary<string, string>
        {
            {"LKSZ01", "LIM CHU KANG" }
        };

        public static Dictionary<string, string> subZoneJurongIsland = new Dictionary<string, string>
        {
            {"WISZ01", "JURONG ISLAND" }
        };

        public static Dictionary<string, string> subZoneSentosaIsland = new Dictionary<string, string>
        {
            {"SISZ01",  "SENTOSA ISLAND"}
        };

        public static Dictionary<string, string> subZoneChangiAirport = new Dictionary<string, string>
        {
            {"CHSZ03",  "CHANGI AIRPORT"}
        };

        public static Dictionary<string, string> subZoneSingle = new Dictionary<string, string>()
        {
            {"LKSZ01", "LIM CHU KANG" },
            {"WISZ01", "JURONG ISLAND" },
            {"SISZ01",  "SENTOSA ISLAND"},
            {"CHSZ03",  "CHANGI AIRPORT"}
        };

        public static Dictionary<string, Coordinate> regionCoordinate = new Dictionary<string, Coordinate>()
        {
            {"CBD", new Coordinate { latitude = 1.297647, longitude = 103.848614  } },
            {"SISZ01", new Coordinate { latitude = 1.250809, longitude = 103.830474  } },
            {"LKSZ01", new Coordinate { latitude = 1.430499, longitude = 103.717046  } },
            {"TS", new Coordinate { latitude = 1.295042, longitude = 103.630748  } },
            {"WISZ01", new Coordinate { latitude = 1.279311, longitude = 103.677026  } },
            {"CHSZ03", new Coordinate { latitude = 1.362640, longitude = 103.991402  } }
        };
    }
}
